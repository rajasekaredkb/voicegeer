package com.example.demo.ui

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.core.view.isVisible
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.demo.R
import com.example.demo.adapter.CartAdapter
import com.example.demo.adapter.CartAdapterSmall
import com.example.demo.databinding.ActivityExpenseListBinding
import com.example.demo.models.CartResponse
import com.example.demo.models.Fruits
import com.example.demo.util.ApiState
import com.example.demo.util.Utilities.showToast
import com.example.demo.viewmodel.ListViewModel
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.android.synthetic.main.toolbarlayout.view.*
import kotlinx.coroutines.flow.collect


@AndroidEntryPoint
class ListActivity : AppCompatActivity(), CartAdapter.OnItemClickListener,


    View.OnClickListener {
    private lateinit var binding: ActivityExpenseListBinding
    private lateinit var postAdapter: CartAdapter
    private lateinit var smallAdapter: CartAdapterSmall
    private val viewModel: ListViewModel by viewModels()
    private var totalPrice: Int = 0
    private var totalOfferPrice: Int = 0
    var off: Int = 0
    var totalQuantity: Int = 0
    private lateinit var context: Context
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_expense_list)
        setContentView(binding.root)
        intializeViews()
        callApi()

    }


    private fun initSmallAdapter() {
        smallAdapter = CartAdapterSmall(ArrayList(), context)
        binding.rvSmall.apply {
            setHasFixedSize(true)
            layoutManager =
                LinearLayoutManager(this@ListActivity, LinearLayoutManager.HORIZONTAL, false)
            adapter = smallAdapter
        }
        //deleteItemFromDb()
    }

    private fun setDataSmall(data: Fruits, pos: Int) {

        smallAdapter.setData(data)
        // smallAdapter.notifyDataSetChanged()
        // binding.rvSmall.smoothScrollToPosition(pos)
        binding.rvSmall.scrollToPosition(pos)

    }


    private fun callApi() {
        val mHashMap = HashMap<String, Any>()
        mHashMap["request_param"] = "Value"
        mHashMap["page"] = "1"
        viewModel.getFruits(mHashMap)

        lifecycleScope.launchWhenStarted {
            viewModel.postStateFlow.collect {
                when (it) {
                    is ApiState.Loading -> {
                    }
                    is ApiState.Failure -> {
                        showToast(context, it.msg.toString())
                    }
                    is ApiState.Success<*> -> {
                        binding.rvExpenseList.isVisible = true
                        val result = it.result as CartResponse
                        //alertsList = result.data
                        val listData: ArrayList<Fruits> = result.data as ArrayList<Fruits>
                        postAdapter.setData(result.data)
                        postAdapter.notifyDataSetChanged()
                        search()
                        // setDataSmallAll(listData,0)
                    }
                    is ApiState.Empty -> {

                    }
                }
            }
        }

    }

    private fun intializeViews() {
        context = this
        postAdapter = CartAdapter(ArrayList(), this, context)
        binding.rvExpenseList.apply {
            setHasFixedSize(true)
            layoutManager = GridLayoutManager(this@ListActivity, 2)
            adapter = postAdapter
        }
        initSmallAdapter()
        binding.topinc.txtoffer.text =
            this.resources.getString(R.string.symbolWithss, "00.00")
        binding.topinc.btnBuy.setOnClickListener {
            if (totalPrice > 1) {
                val intent = Intent(this, CheckoutActivity::class.java)
                intent.putExtra("total", totalPrice.toString())
                intent.putExtra("savings", off.toString())
                startActivity(intent)

            } else {
                Toast.makeText(this, "Your Cart Is empty", Toast.LENGTH_SHORT).show()
            }

        }
    }


    private fun search() {
        binding.searchView.setOnQueryTextListener(object :
            SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(p0: String?): Boolean {
                return true
            }

            override fun onQueryTextChange(query: String?): Boolean {
                postAdapter.filter.filter(query)

                return true
            }
        })

    }

    override fun onClick(view: View?) {
        when (view?.id) {
        }
    }


    override fun onItemClicked(data: Fruits, pos: Int) {

        setDataSmall(data, pos)
        totalPrice += Integer.parseInt(data.offprice)
        totalOfferPrice += Integer.parseInt(data.price)
        totalQuantity++
        binding.topinc.toolBar.txtTotal.text = totalPrice.toString()
        binding.topinc.toolBar.rtxPricetag.text = totalQuantity.toString()
        off = totalOfferPrice - totalPrice
        binding.topinc.toolBar.txtoffer.text =
            this.resources.getString(R.string.symbolWithss, off.toString())

    }


    override fun onItemText(data: TextView, id: String) {

    }

}



