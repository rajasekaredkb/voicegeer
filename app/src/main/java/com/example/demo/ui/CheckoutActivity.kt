package com.example.demo.ui

import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.example.demo.R
import com.example.demo.databinding.ActivityCheckoutBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class CheckoutActivity : AppCompatActivity() {
    private lateinit var binding: ActivityCheckoutBinding
    private lateinit var total: String
    private lateinit var savings: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityCheckoutBinding.inflate(layoutInflater)
        setContentView(binding.root)

        try {
            setView()
            initialize()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setView() {


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            android.R.id.home -> {
                finish()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun initialize() {
        total = intent.getStringExtra("total").toString()
        savings = intent.getStringExtra("savings").toString()
        binding.textTotalPrice.text = this.resources.getString(R.string.symbolWith, "150")
        binding.textTotalPrices.text = this.resources.getString(R.string.symbolWith, total)
        binding.textPrice.text = this.resources.getString(
            R.string.symbolWith,
            "$savings total savings"
        )
        binding.ivBack.setOnClickListener { finish() }

    }
}