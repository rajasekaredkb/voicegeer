package com.example.demo.network

import com.example.demo.models.CardResponse
import com.example.demo.models.CartResponse
import com.example.demo.models.ProfileResponse
import retrofit2.http.Field
import retrofit2.http.FormUrlEncoded
import retrofit2.http.POST

interface ApiService {
    @POST("v3/b9971011-0940-4a7a-8050-f156e65c5c81")
    @FormUrlEncoded
    suspend fun getFruits(
        @Field("demo") data: String?
    ): CartResponse

}