package com.example.demo.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.demo.databinding.EachRowBinding
import com.example.demo.models.Fruits
import java.util.*

class CartAdapter(
    private var results: List<Fruits>,
    private val listener: OnItemClickListener,
    var context: Context
) : RecyclerView.Adapter<CartAdapter.MyViewHolder>(), Filterable {
    private lateinit var eachRowBinding: EachRowBinding
    var resultsFilterList = ArrayList<Fruits>()


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        eachRowBinding = EachRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(eachRowBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val data = results[position]
        holder.eachRowBinding.expenseResponse = data
        holder.eachRowBinding.executePendingBindings()
        holder.bind(data, listener, context, position)
    }

    override fun getItemCount(): Int = resultsFilterList.size


    class MyViewHolder(var eachRowBinding: EachRowBinding) :
        RecyclerView.ViewHolder(eachRowBinding.root) {
        fun bind(
            data: Fruits,
            listener: OnItemClickListener,
            context: Context,
            pos: Int
        ) {

            Glide.with(context)
                .load(data.image)
                .into(eachRowBinding.imgReceipt)

            listener.onItemText(eachRowBinding.rtxOffPricetag, data.id)
            eachRowBinding.rtxoffPrice.paintFlags =
                eachRowBinding.rtxoffPrice.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG

            eachRowBinding.cvExpenseList.setOnClickListener {
                // listener.onItemClicked(data)
            }

            eachRowBinding.buttonAdd.setOnClickListener {
                listener.onItemClicked(data, pos)
            }

        }
    }

    interface OnItemClickListener {
        fun onItemClicked(data: Fruits, pos: Int)
        fun onItemText(data: TextView, id: String)
    }

    fun setData(expensesResponse: List<Fruits>) {
        this.results = expensesResponse
        resultsFilterList = results as ArrayList<Fruits>
    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                if (charSearch.isEmpty()) {
                    resultsFilterList = results as ArrayList<Fruits>
                } else {
                    val resultList = ArrayList<Fruits>()
                    for (row in results) {

                        if (row.name.lowercase(Locale.getDefault())
                                .contains(constraint.toString().lowercase(Locale.getDefault()))
                        ) {

                            println("Deiii Na inga irukkan da---> " + row.name + "--" + constraint)
                            resultList.add(row)
                        }

                    }
                    resultsFilterList = resultList
                }
                val filterResults = FilterResults()
                filterResults.values = resultsFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                resultsFilterList = results!!.values as ArrayList<Fruits>
                println("Deiii Na inga irukkan da size---> " + resultsFilterList.size)
                notifyDataSetChanged()
            }
        }
    }
}
