package com.example.demo.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Filter
import android.widget.Filterable
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.demo.databinding.SmallRowBinding
import com.example.demo.models.Fruits

class CartAdapterSmall(
    private var results: ArrayList<Fruits>,
    var context: Context
) : RecyclerView.Adapter<CartAdapterSmall.MyViewHolder>(), Filterable {
    private lateinit var eachRowBinding: SmallRowBinding
    var resultsFilterList = ArrayList<Fruits>()

    init {
        resultsFilterList = results
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        eachRowBinding = SmallRowBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return MyViewHolder(eachRowBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, pos: Int) {
        val data = results[pos]
        holder.eachRowBinding.expenseResponse = data
        holder.eachRowBinding.executePendingBindings()
        holder.bind(data, context, pos)
    }

    override fun getItemCount(): Int = resultsFilterList.size


    class MyViewHolder(var eachRowBinding: SmallRowBinding) :
        RecyclerView.ViewHolder(eachRowBinding.root) {
        fun bind(
            data: Fruits,
            context: Context,
            pos: Int
        ) {

            Glide.with(context)
                .load(data.image)
                .into(eachRowBinding.imgReceipt)

            // listener.onItemText(eachRowBinding.rtxOffPricetag, data.id)
            eachRowBinding.rtxPricetag.text = data.qty.toString()
            eachRowBinding.Constroot.setOnClickListener {
                //listener.onItemClicked(data,pos)
            }


        }
    }


    fun setData(data: Fruits) {

        val listData: ArrayList<Int> = ArrayList()
        val listData1: ArrayList<Int> = ArrayList()
        if (results.size == 0) {
            data.apply { data.qty = 1 }
            results.add(data)
            notifyDataSetChanged()
        } else {
            for (i in 0 until results.size) {

                if (results[i].id == data.id) {
                    listData.add(i)
                } else {
                    listData1.add(i)
                }
            }

            if (listData.size == 1) {
                val dataval = data.apply {
                    qty += 1
                }
                results[listData[0]] = dataval
                notifyItemChanged(listData[0])
            } else {
                results.add(data)
                data.apply { data.qty = 1 }
                notifyDataSetChanged()
            }

        }
    }

    fun setDataAll(data: ArrayList<Fruits>) {
        results.addAll(data)
        notifyDataSetChanged()

    }

    override fun getFilter(): Filter {
        return object : Filter() {
            override fun performFiltering(constraint: CharSequence?): FilterResults {
                val charSearch = constraint.toString()
                resultsFilterList = if (charSearch.isEmpty()) {
                    results
                } else {
                    val resultList = ArrayList<Fruits>()
                    for (row in results) {
                        if (row.name!!.toLowerCase().contains(
                                constraint.toString().toLowerCase()
                            )
                        ) {
                            resultList.add(row)
                        }
                    }
                    resultList
                }
                val filterResults = FilterResults()
                filterResults.values = resultsFilterList
                return filterResults
            }

            override fun publishResults(constraint: CharSequence?, results: FilterResults?) {
                resultsFilterList = results?.values as ArrayList<Fruits>
                notifyDataSetChanged()
            }
        }
    }
}
