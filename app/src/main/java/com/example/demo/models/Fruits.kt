package com.example.demo.models

import com.google.gson.annotations.SerializedName

data class Fruits(
    @SerializedName("name")
    val name: String,
    @SerializedName("image")
    val image: String?,
    @SerializedName("price")
    val price: String?,
    @SerializedName("offprice")
    val offprice: String?,
    @SerializedName("id")
    val id: String,
    @SerializedName("qty")
    var qty: Int=1
)
