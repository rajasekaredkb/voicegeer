package com.example.demo.models

import com.google.gson.annotations.SerializedName

data class CartResponse(
    @SerializedName("fruits")
    val data: List<Fruits>
)
